# Setup development environment
1. Create an environment file `cp template.env .env`.
2. Open the `.env` file, created in the previous step, add the required settings for your environment and remove any unnecessary variables.
3. Use `flask run` to run the development server.
