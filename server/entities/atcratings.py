class ATCRating:
    @property
    def facilities(self): 
        return {
            2:  'GND/DEL',
            3:  'TWR',
            4:  'APP',
            5:  'CTR',
            7:  'FSS'
        }
    @property
    def ratings(self): 
        return {
            2:  'S1',
            3:  'S2',
            4:  'S3',
            5:  'C1',
            7:  'C3',
            8:  'I1',
            10: 'I3',
            11: 'SUP',
            12: 'ADMIN',
        }
    @staticmethod
    def get_rating(id):
        ratings = ATCRating()
        return ratings.ratings[int(id)]

    @staticmethod
    def get_facility(id):
        ratings = ATCRating()
        return ratings.facilities[int(id)]

    @staticmethod
    def get_avail_facilities(limit):
        ratings = ATCRating()
        return [[key, value] for key, value in ratings.facilities.items() if key <= limit]