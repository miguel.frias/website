import logging
from os import environ
from dotenv import load_dotenv, find_dotenv

load_dotenv()

mail_use_tls = environ.get('MAIL_USE_TLS', 'False')

class _Config:
    ADS_ROOT              = environ.get('ADS_ROOT', '')
    APP_SECRET_KEY        = environ.get('APP_SECRET_KEY', 'secret-key')
    AVWX_TOKEN            = environ.get('AVWX_TOKEN', '')
    CELERY_BROKER_URL     = environ.get('CELERY_BROKER_URL', 'redis://')
    CHARTFOX_TOKEN        = environ.get('CHARTFOX_TOKEN', '')
    DATA_SECRET_KEY       = environ.get('DATA_SECRET_KEY', '')
    DATE_FORMAT           = environ.get('DATE_FORMAT', r'%Y-%m-%d %H:%M:%S')
    DISCORD_SECRET        = environ.get('DISCORD_SECRET', '')
    EVENT_STORE_PATH      = environ.get('EVENT_STORE_PATH', '.store')
    HOST_URL              = environ.get('HOST_URL', 'http://localhost:5000')
    MAIL_DEFAULT_SENDER   = environ.get('MAIL_DEFAULT_SENDER', '')
    MAIL_PASSWORD         = environ.get('MAIL_PASSWORD', '')
    MAIL_PORT             = environ.get('MAIL_PORT', 25)
    MAIL_SERVER           = environ.get('MAIL_SERVER', 'localhost')
    MAIL_USE_TLS          = mail_use_tls.lower() == 'true'
    MAIL_USERNAME         = environ.get('MAIL_NOREPLY', 'noreply@portugal-vacc.org')
    MONGO_URI             = environ.get('MONGO_URI', 'mongodb://localhost:27017/pvacc')
    MONGO_URI2            = environ.get('MONGO_URI2', 'mongodb://localhost:27017')
    MYSQL_DB              = environ.get('MYSQL_DB', '')
    MYSQL_HOST            = environ.get('MYSQL_HOST', 'localhost')
    MYSQL_PASS            = environ.get('MYSQL_PASS', '')
    MYSQL_USER            = environ.get('MYSQL_USER', '')
    SSO_API_URL           = environ.get('SSO_API_URL', 'https://ssoauth.portugal-vacc.org')
    TS3_SERVER_ADMIN      = environ.get('TS3_SERVER_ADMIN', '')
    TS3_SERVER_ADMIN_PASS = environ.get('TS3_SERVER_ADMIN_PASS', '')
    TS3_SERVER_HOST       = environ.get('TS3_SERVER_HOST', '')
class _DevelopmentConfig(_Config):
    logging.basicConfig(level=logging.DEBUG)
    DEBUG = True

class _ProductionConfig(_Config):
    logging.basicConfig(level=logging.INFO)

_CONFIG = {
    'production': _ProductionConfig,
    'development': _DevelopmentConfig
}

def config_for(name):
    if name in _CONFIG:
        return _CONFIG[name]

    logging.warn(f'using default configuration:attempted to use invalid configuration "{name}"')

    return _Config
