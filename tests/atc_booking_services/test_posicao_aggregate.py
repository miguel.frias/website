from datetime import datetime, timedelta
from unittest import TestCase
from uuid import uuid4

from src.kernel.messages import MessageBuilder

from src.atc_bookings_service import MarcarSessao, CancelarSessao, SessaoMarcada
from src.atc_bookings_service.posicao_aggregate import Posicao

class TestPosicaoSemSessoesMarcadas(TestCase):
    def setUp(self):
        posicao = Posicao(uuid4())

        self.posicao = posicao

    def test_adicionarSessao(self):
        posicao = self.posicao

        cmd = self._marcar_sessao(hours=2)
        posicao.handle(cmd)

        self.assertEqual(len(posicao.uncommitted), 1)
        evt = posicao.uncommitted[-1]
        self.assertIsInstance(evt, SessaoMarcada)
        self.assertEqual(evt.inicio, cmd.inicio)
        self.assertEqual(evt.fim, cmd.fim)

    def test_adicionarSessaoInferiorA30Minutos(self):
        posicao = self.posicao

        cmd = self._marcar_sessao(minutes=29)
        posicao.handle(cmd)

        self.assertEqual(len(posicao.uncommitted), 0)

    def _marcar_sessao(self, inicio=datetime.utcnow(), **duration):
        return MessageBuilder() \
            .aggregate(self.posicao.id, -1) \
            .type(MarcarSessao) \
            .kwargs(
                sessao_id=uuid4(),
                controller_id=uuid4(),
                inicio=inicio,
                fim=inicio + timedelta(**duration),
            ) \
            .build()

class TestPosicaoComSessoesMarcadas(TestCase):
    def setUp(self):
        posicao = Posicao(uuid4())
        _inicio = datetime.utcnow()
        _fim = _inicio + timedelta(hours=2)
        sessao = SessaoMarcada(
            None, None, None,
            posicao.id, 0,
            uuid4(), uuid4(),
            _inicio, _fim)
        posicao.load_from_history([sessao])

        self.sessao = sessao
        self.posicao = posicao

    def test_depoisDeCancelar_consegueMarcar(self):
        sessao = self.sessao
        posicao = self.posicao

        cancelar = self._cancelar_sessao(sessao.sessao_id)
        marcar = self._marcar_sessao(sessao.inicio, hours=2)
        posicao.handle(cancelar)
        posicao.commit()

        posicao.handle(marcar)

        self.assertEqual(len(posicao.uncommitted), 1)

    def test_iniciarAntes_terminarDurante(self):
        sessao = self.sessao
        posicao = self.posicao

        marcar = self._marcar_sessao(sessao.inicio + timedelta(minutes=-30), hours=1)
        posicao.handle(marcar)

        self.assertEqual(len(posicao.uncommitted), 0)

    def test_iniciarAntes_terminarDepois(self):
        sessao = self.sessao
        posicao = self.posicao

        marcar = self._marcar_sessao(sessao.inicio + timedelta(minutes=-30), hours=3)
        posicao.handle(marcar)

        self.assertEqual(len(posicao.uncommitted), 0)

    def test_iniciarDurante_terminarDepois(self):
        sessao = self.sessao
        posicao = self.posicao

        marcar = self._marcar_sessao(sessao.inicio + timedelta(minutes=30), hours=2)
        posicao.handle(marcar)

        self.assertEqual(len(posicao.uncommitted), 0)

    def test_iniciarDepois(self):
        sessao = self.sessao
        posicao = self.posicao

        marcar = self._marcar_sessao(sessao.inicio + timedelta(hours=2), hours=2)
        posicao.handle(marcar)

        self.assertEqual(len(posicao.uncommitted), 1)

    def test_terminarAntes(self):
        sessao = self.sessao
        posicao = self.posicao

        marcar = self._marcar_sessao(sessao.inicio + timedelta(hours=-2), hours=2)
        posicao.handle(marcar)

        self.assertEqual(len(posicao.uncommitted), 1)

    def _marcar_sessao(self, inicio=datetime.utcnow(), **duration):
        return MessageBuilder() \
            .aggregate(self.posicao.id, -1) \
            .type(MarcarSessao) \
            .kwargs(
                sessao_id=uuid4(),
                controller_id=uuid4(),
                inicio=inicio,
                fim=inicio + timedelta(**duration),
            ) \
            .build()

    def _cancelar_sessao(self, sessao_id):
        return MessageBuilder() \
            .aggregate(self.posicao.id, 0) \
            .type(CancelarSessao) \
            .kwargs(sessao_id=sessao_id) \
            .build()
