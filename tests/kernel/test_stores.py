import tempfile
import random
import shutil
from collections import defaultdict
from dataclasses import dataclass
from unittest import TestCase
from uuid import uuid4

from src.kernel.stores import FileEventStore, AggregateVersionMismatch
from src.kernel.buses import MemoryBus
from src.kernel.aggregate_root import AggregateRoot
from src.kernel.messages import MessageBuilder, event
from src.kernel.repositories import Repository

from tests.kernel.test_bus import TestBus

@event
class TestEvent:
    pass

@dataclass
class TestAggregateRoot(AggregateRoot):
    def __post_init__(self):
        super().__post_init__()

        self.applied = False

    def test(self):
        builder = MessageBuilder().type(TestEvent).aggregate(self.id)
        self.apply_change(builder.build())

    def apply(self, evt: TestEvent):
        self.applied = True

class TestFileEventStore(TestCase):
    def setUp(self):
        self._tmp_dir = tempfile.mkdtemp()
        store = FileEventStore(MemoryBus(), self._tmp_dir)
        self.repo = Repository(TestAggregateRoot, store)

    def tearDown(self):
        shutil.rmtree(self._tmp_dir)

    def test_store_does_not_fail(self):
        obj = self.repo.get(uuid4())

        obj.test()
        self.assertTrue(obj.applied)

        self.repo.save(obj, -1)
        stored = self.repo.get(obj.id)

        self.assertEqual(obj.__class__, stored.__class__)
        self.assertEqual(obj.id, stored.id)
        self.assertEqual(obj.applied, stored.applied)

    def test_storeWrongVersion_raisesMismatch(self):
        obj = self.repo.get(uuid4())
        obj.test()
        obj.test()
        obj.test()
        self.repo.save(obj, -1)

        with self.assertRaises(AggregateVersionMismatch):
            self.repo.save(obj, -1)
        with self.assertRaises(AggregateVersionMismatch):
            self.repo.save(obj, 0)
        with self.assertRaises(AggregateVersionMismatch):
            self.repo.save(obj, 1)

        self.repo.save(obj, 2)

    def test_storeExistingAggregate(self):
        obj = self.repo.get(uuid4())
        obj.test()
        self.repo.save(obj, -1)

        obj = self.repo.get(obj.id)
        obj.test()
        self.repo.save(obj, 0)

class TestReplayEvents(TestCase):
    N_OF_TEST_AGGREGATES = 3
    MIN_TEST_EVTS = 3
    MAX_TEST_EVTS = 10

    def setUp(self):
        bus = TestBus()
        store = FileEventStore(bus, tempfile.mkdtemp())
        repo = Repository(TestAggregateRoot, store)

        # generate 3 aggregates
        for _ in range(TestReplayEvents.N_OF_TEST_AGGREGATES):
            obj = repo.get(uuid4())

            # generate 3 to 10 events per aggregate
            min = TestReplayEvents.MIN_TEST_EVTS
            max = TestReplayEvents.MAX_TEST_EVTS
            for _ in range(random.randint(min, max)):
                obj.test()

            repo.save(obj, -1)

        self.store = store
        self.bus = bus
        bus.messages.clear()

    def test_replaysEventsInVersionOrderPerAggregate(self):
        bus = self.bus
        store = self.store

        store.replay(bus)

        aggregate_versions = defaultdict(lambda: -1)
        for msg in bus.messages:
            aggregate_versions[msg.aggregate_id] += 1
            last_version = aggregate_versions[msg.aggregate_id]
            self.assertEqual(last_version, msg.aggregate_version)

        processed_agg = len(aggregate_versions.keys())
        self.assertEqual(processed_agg, TestReplayEvents.N_OF_TEST_AGGREGATES)

        for version in aggregate_versions.values():
            self.assertGreaterEqual(version, TestReplayEvents.MIN_TEST_EVTS - 1)
