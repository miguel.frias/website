from unittest import TestCase

from src.kernel.buses import TestBus

class TestMessage: pass

class TestTestBus(TestCase):
    def setUp(self):
        bus = TestBus()
        msg = TestMessage()
        bus.publish(msg)
        self.bus = bus

    def test_nothingElse_raisesAssertionError(self):
        with self.assertRaises(AssertionError):
            self.bus.assert_nothing_else()

    def test_assertReceived_raisesAssertionError(self):
        with self.assertRaises(AssertionError):
            self.bus.assert_received_type(None)

    def test_assertReceived_returnsNone(self):
        self.bus.assert_received_type(TestMessage)
        self.bus.assert_nothing_else()

