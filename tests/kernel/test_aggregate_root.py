from dataclasses import dataclass
from functools import singledispatchmethod
from unittest import TestCase
from uuid import UUID, uuid4

from src.kernel.aggregate_root import AggregateRoot

@dataclass(frozen=True)
class TestEventApply: pass

@dataclass(frozen=True)
class TestEventNoApply: pass

class TestAggregateRoot(AggregateRoot):
    def __init__(self, id: UUID):
        super().__init__(id)
        self._applied = list()

    @property
    def applied(self):
        return tuple(self._applied)

    def test_apply(self):
        self.apply_change(TestEventApply())

    def test_no_apply(self):
        self.apply_change(TestEventNoApply())

    @singledispatchmethod
    def apply(self, evt):
        pass

    @apply.register
    def _(self, evt: TestEventApply):
        self._applied.append(evt)

class TestsAggregateRoot(TestCase):
    def setUp(self):
        self.aggregate_root = TestAggregateRoot(uuid4())

    def test_apply_event(self):
        ar = self.aggregate_root
        ar.test_apply()
        self.assertEqual(ar.applied, ar.uncommitted)
        self.assertGreater(len(ar.applied), 0)

    def test_no_apply(self):
        ar = self.aggregate_root
        ar.test_no_apply()
        self.assertGreater(len(ar.uncommitted), 0)
        self.assertEqual(len(ar.applied), 0)

    def test_commit(self):
        ar = self.aggregate_root
        ar.test_no_apply()
        committed = ar.uncommitted
        ar.commit()
        self.assertNotEqual(committed, ar.uncommitted)
