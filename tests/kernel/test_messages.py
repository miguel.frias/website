from unittest import TestCase
from uuid import uuid4

from src.kernel.messages import MessageBuilder, command, event

@command
class TestCommand:
    field: str

@event
class TestEvent:
    field: str

class TestControllerMessageBuilding(TestCase):
    def setUp(self):
        self.cmd_id = uuid4()
        self.builder = MessageBuilder(self.cmd_id) \
            .type(TestCommand) \
            .args('value')

    def test_createCommand(self):
        cmd = self.builder \
            .aggregate(uuid4()) \
            .build()

        self.assertEqual(cmd.message_id, self.cmd_id)
        self.assertEqual(cmd.correlation_id, cmd.message_id)
        self.assertEqual(cmd.causation_id, cmd.message_id)
        self.assertEqual(cmd.aggregate_version, -1)
        self.assertIsInstance(cmd.field, str)

    def test_mutateCommand(self):
        cmd = self.builder \
            .aggregate(uuid4(), 4) \
            .build()

        self.assertEqual(cmd.message_id, self.cmd_id)
        self.assertEqual(cmd.correlation_id, cmd.message_id)
        self.assertEqual(cmd.causation_id, cmd.message_id)
        self.assertNotEqual(cmd.aggregate_version, -1)
        self.assertIsInstance(cmd.field, str)

class TestAggregateMessageBuilding(TestCase):
    def setUp(self):
        self.cmd = MessageBuilder() \
            .type(TestCommand) \
            .aggregate(uuid4()) \
            .args('value') \
            .build()
        self.builder = MessageBuilder().type(TestEvent)

    def test_withArgs(self):
        cmd = self.cmd
        evt = self.builder \
            .caused_by(self.cmd) \
            .args(self.cmd.field) \
            .build()
        evt2 = self.builder \
            .caused_by(evt) \
            .args(evt.field) \
            .build()

        self.assertIsNotNone(evt.message_id)
        self.assertIsNotNone(evt2.message_id)
        self.assertEqual(evt.causation_id, self.cmd.message_id)
        self.assertEqual(evt2.causation_id, evt.message_id)
        self.assertEqual(evt.correlation_id, cmd.correlation_id)
        self.assertEqual(evt2.correlation_id, cmd.correlation_id)
        self.assertIsInstance(cmd.field, str)
        self.assertEqual(cmd.field, evt.field)
        self.assertEqual(cmd.field, evt2.field)

    def test_withKwargs(self):
        cmd = self.cmd
        evt = self.builder \
            .caused_by(self.cmd) \
            .kwargs(field=self.cmd.field) \
            .build()
        evt2 = self.builder \
            .caused_by(evt) \
            .kwargs(field=evt.field) \
            .build()

        self.assertIsNotNone(evt.message_id)
        self.assertIsNotNone(evt2.message_id)
        self.assertEqual(evt.causation_id, self.cmd.message_id)
        self.assertEqual(evt2.causation_id, evt.message_id)
        self.assertEqual(evt.correlation_id, cmd.correlation_id)
        self.assertEqual(evt2.correlation_id, cmd.correlation_id)
        self.assertIsInstance(cmd.field, str)
        self.assertEqual(cmd.field, evt.field)
        self.assertEqual(cmd.field, evt2.field)
