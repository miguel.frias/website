from unittest import TestCase

from src.canonical_model.vatsim_ratings import Rating, PosicaoAtc

class TestDomainKernel(TestCase):
    def test_ratingFromStr(self):
        self.assertEqual(Rating['OBS'], Rating.OBS)

    def test_ratingFromInt(self):
        self.assertEqual(Rating(1), Rating.OBS)

    def test_posicaoFromStr(self):
        self.assertEqual(PosicaoAtc['CTR'], PosicaoAtc.CTR)
