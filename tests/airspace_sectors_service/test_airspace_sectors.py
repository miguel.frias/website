from functools import partial
from tempfile import mkdtemp
from unittest import TestCase
from uuid import uuid4

from src.kernel.stores import FileEventStore
from src.kernel.buses import MemoryBus
from src.kernel.messages import MessageBuilder

from src.airspace_sectors_service import LPPC, LPPO, RivCommandHandlers, CriarSector, AlterarFrequenciaSector, SectorCriado, FrequenciaSectorAlterada

class TestBus(MemoryBus):
    def __init__(self):
        super().__init__()
        self.messages = []

    def publish(self, message):
        super().publish(message)
        self.messages.append(message)

    def assert_received_type(self, msg_type):
        for message in self.messages:
            if type(message) == msg_type:
                self.messages.remove(message)
                return True
        return False

    def assert_nothing_else(self):
        return len(self.messages) == 0

class TestGivenAFirWithNoSectors(TestCase):
    def setUp(self):
        bus = TestBus()
        store = FileEventStore(bus, mkdtemp())
        RivCommandHandlers.register(bus, store)
        self.bus = bus

    def test_when_addingASector_sectorCriadoEventIsRaised(self):
        bus = self.bus

        cmd = MessageBuilder() \
            .type(CriarSector) \
            .aggregate(LPPC) \
            .args(uuid4(), 'Lisboa Tower', 'LPPT_TWR', 'TWR', 118.1) \
            .build()

        bus.send(cmd)
        bus.assert_received_type(SectorCriado)
        bus.assert_nothing_else()
