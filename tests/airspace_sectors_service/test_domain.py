from unittest import TestCase
from uuid import uuid4

from src.kernel.messages import MessageBuilder
from src.airspace_sectors_service.rivs_aggregate import Riv
from src.airspace_sectors_service import CriarSector, AlterarFrequenciaSector

class TestRivsDomain(TestCase):
    def setUp(self):
        riv = Riv(uuid4())
        sector = MessageBuilder() \
            .type(CriarSector) \
            .aggregate(riv.id, -1) \
            .kwargs(
                sector_id=uuid4(),
                nome='Lisboa Tower',
                callsign='LPPT_TWR',
                posicao='TWR',
                frequencia=118.1,
            ) \
            .build()
        riv.handle(sector)
        riv.commit()

        self.riv = riv
        self.lppt_twr_id = sector.sector_id

    def test_createTwoSectorsOnSameFrequency_doesNotWork(self):
        riv = self.riv

        sector_2 = MessageBuilder() \
            .type(CriarSector) \
            .aggregate(riv.id, 0) \
            .kwargs(
                sector_id=uuid4(),
                nome='Porto Tower',
                callsign='LPPR_TWR',
                posicao='TWR',
                frequencia=118.1,
            ) \
            .build()
        riv.handle(sector_2)

        self.assertEqual(len(riv.uncommitted), 0)

    def test_afterChangingOccupiedFrequency_canAddNewSectorOnOldFrequency(self):
        riv = self.riv
        lppt_twr_id = self.lppt_twr_id
        sector_1_altera_freq = MessageBuilder() \
            .type(AlterarFrequenciaSector) \
            .aggregate(riv.id, 0) \
            .kwargs(
                sector_id=lppt_twr_id,
                frequencia=118.105,
            ) \
            .build()
        riv.handle(sector_1_altera_freq)
        sector_2 = MessageBuilder() \
            .type(CriarSector) \
            .aggregate(riv.id, 0) \
            .kwargs(
                sector_id=uuid4(),
                nome='Porto Tower',
                callsign='LPPR_TWR',
                posicao='TWR',
                frequencia=118.1,
            ) \
            .build()
        riv.handle(sector_2)

        self.assertEqual(len(riv.uncommitted), 2)
