from abc import ABC, abstractmethod
from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
from functools import singledispatchmethod
from typing import Callable
from uuid import UUID

class Repository:
    def __init__(self, factory: Callable[[UUID], 'AggregateRoot'], store):
        self._factory = factory
        self._store = store

    def get(self, id: UUID) -> 'AggregateRoot':
        obj = self._factory(id)
        history = self._store.get_events(id)
        obj.load_from_history(history)
        return obj

    def save(self, item: 'AggregateRoot', expected_version: int):
        self._store.save_events(item.id, item.uncommitted, expected_version)
        item.commit()
