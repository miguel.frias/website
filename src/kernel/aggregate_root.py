from abc import ABC, abstractmethod
from dataclasses import dataclass
from uuid import UUID

@dataclass
class AggregateRoot(ABC):
    id: UUID

    def __post_init__(self):
        self._changes = list()

    def handle(self, evt):
        pass

    def apply(self, evt):
        pass

    @property
    def uncommitted(self):
        return tuple(self._changes)

    def commit(self):
        self._changes.clear()

    def apply_change(self, event, _is_new=True):
        self.apply(event)
        if _is_new:
            self._changes.append(event)

    def load_from_history(self, history):
        for event in history:
            self.apply_change(event, _is_new=False)
