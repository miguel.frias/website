import os
import sys
from flask import Flask
from flask.json import JSONEncoder
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_pymongo import PyMongo
from flask_login import LoginManager
from flask_mail import Mail
from config import config_for
from bson.objectid import ObjectId
from flaskext.markdown import Markdown
from uuid import UUID
from datetime import datetime

from src.kernel.buses import MemoryBus
from src.kernel.stores import MongoEventStore

app = Flask(__name__, instance_relative_config=False)
Bootstrap(app)
Markdown(app)
moment = Moment(app)
app.login_manager = LoginManager()
app.login_manager.init_app(app)

config_name = os.environ.get('FLASK_ENV', 'production')
config = config_for(config_name)

app.config.from_object(config)
app.secret_key = app.config['APP_SECRET_KEY']

app.bus = MemoryBus()
app.db = PyMongo(app).db
app.mail = Mail(app)

app.bus = MemoryBus()
app.store = MongoEventStore(app.bus, app.config['MONGO_URI2'])

class CustomJsonEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        elif isinstance(obj, UUID):
            return str(obj)
        elif isinstance(obj, datetime):
            time_format = r'%Y-%m-%dT%H:%M'
            return datetime.strftime(obj, time_format)
        return super().default(obj)
app.json_encoder = CustomJsonEncoder

with app.app_context():
    from . import routes

    ## @TODO: migrate the routes below
    from . import auth
    app.register_blueprint(auth.bp)

    from . import events
    app.register_blueprint(events.bp)

    from . import airports
    app.register_blueprint(airports.bp)

    from . import controllers
    app.register_blueprint(controllers.bp)

    from . import atcmentoring
    app.register_blueprint(atcmentoring.bp)

    from . import images
    app.register_blueprint(images.bp)

    from . import teamspeak
    app.register_blueprint(teamspeak.bp, url_prefix='/ts')

    from . import airspace_sectors
    app.register_blueprint(airspace_sectors.bp, url_prefix='/airspace_sectors')

app.store.replay(app.bus)
