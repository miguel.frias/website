from bson.objectid import ObjectId
from datetime import datetime
from dateutil.relativedelta import relativedelta

def vatsim_user_exists(db, vatsim_user):
    return get_user_for_vatsim_user(db, vatsim_user) is not None

def get_users(db):
    return db['users'].find()

def get_user_for_id(db, _id):
    return db['users'].find_one({'_id': ObjectId(_id)})

def get_user_for_vatsim_user(db, vatsim_user):
    return db['users'].find_one({'cid': vatsim_user['cid']})

def get_user_for_cid(db, cid):
    return db['users'].find_one({'cid': cid})

def get_user_for_vatsim_auth_resp(db, auth_resp):
    auth_resp = _parse_auth_resp(auth_resp)
    user      = db['users'].find_one({'cid': auth_resp['cid']})

    if user:
        db['users'].update_one({'_id': user['_id']}, {'$set': {**auth_resp}})
    else:
        default_user = {
            'groups'              : [],
            'atc_certifications'  : [],
            'is_staff'            : False,
            'is_atcmentor'        : False,
            'is_director'         : False,
            'is_training_director': False,
            'is_webmaster'        : False,
        }
        user   = {**default_user, **auth_resp}
        insert = db['users'].insert_one(user)
        user   = {'_id': insert.inserted_id, **user}

    db['users'].update_one({'_id': user['_id']}, {'$set': {'last_update': datetime.utcnow()}})

    return user

def get_controller_stats(db):
    users = list(get_users(db))

    non_controller_r = {'INA', 'OBS'}
    controllers = [u for u in users if u['atc_rating'] not in non_controller_r]
    controllers = len(controllers)

    s1_or_lower_ratings = {'INA', 'OBS', 'S1'}
    s1_or_lower = [u for u in users if u['atc_rating'] in s1_or_lower_ratings]
    s1_or_lower = len(s1_or_lower)

    s2_to_s3_ratings = {'S2', 'S3'}
    s2_to_s3 = [u for u in users if u['atc_rating'] in s2_to_s3_ratings]
    s2_to_s3 = len(s2_to_s3)

    lower_c1_r = {*s1_or_lower_ratings, *s2_to_s3_ratings}
    c1_or_higher = [u for u in users if u['atc_rating'] not in lower_c1_r]
    c1_or_higher = len(c1_or_higher)

    last_2_months = datetime.utcnow() - relativedelta(months=2)
    login_2_months = [u for u in users if u['last_update'] < last_2_months]
    login_2_months = len(login_2_months)

    def percent(value, total=len(users)):
        return int((100 * value) / total)

    return {
        's1_or_lower': {
            'count': s1_or_lower,
            'percent': percent(s1_or_lower),
        },
        's2_to_s3': {
            'count': s2_to_s3,
            'percent': percent(s2_to_s3),
            },
        'c1_or_higher': {
            'count': c1_or_higher,
            'percent': percent(c1_or_higher),
            },
        'login_2_months': {
            'count': login_2_months,
            'percent': percent(login_2_months),
        },
        'total': controllers,
    }

def add_user(db, user):
    insert = db['users'].insert_one(user)
    user['_id'] = insert.inserted_id
    return user

def save(db, user):
    return db['users'].update_one({'_id': user['_id']}, {'$set': user})

def _parse_auth_resp(auth_resp):
    return {
        **auth_resp['data'],
        'pilot_ratings': _parse_pilot_ratings(auth_resp)
    }

def _parse_pilot_ratings(auth_resp):
    binary = format(auth_resp['data']['vatsim']['pilotrating']['id'], 'b')
    ratings = [f'P{i}' for i in range(1, 8)]
    return [
        ratings[i]
        for i, rating in enumerate(binary)
        if int(rating) & 1
    ]

def _update_permissions_from_other_collections(db, user):
    lookup      = {'cid': user['cid']}
    staff_user  = db['staffs'].find_one(lookup)
    mentor_user = db['atcmentors'].find_one(lookup)

    webmasters   = ['Webmaster', 'Webmaster Advisor']
    is_staff     = staff_user is not None
    is_mentor    = mentor_user is not None
    is_webmaster = is_staff and staff_user['role'] in webmasters
    is_director  = is_staff and staff_user['role'] == 'Director'
    is_tdirector = is_staff and staff_user['role'] == 'Training Director'
    changes = {
        'atc_certifications'  : user.get('atc_certifications', []),
        'last_update'         : datetime.now(),
        'is_staff'            : is_staff,
        'is_atcmentor'        : is_mentor,
        'is_director'         : is_director,
        'is_training_director': is_tdirector,
        'is_webmaster'        : is_webmaster,
    }
    db['users'].update_one(lookup, {'$set': changes})
