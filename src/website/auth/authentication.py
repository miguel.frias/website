from datetime import datetime

def create_user(vatsim_user):
    user = {
        'cid'               : vatsim_user['cid'],
        'atc_certifications': [],
        'groups'            : [],
    }
    update_vatsim_info(user, vatsim_user)

    return user

def update_vatsim_info(user, vatsim_user):
    vatsim_info = {
        'name'              : vatsim_user['personal']['name_full'],
        'email'             : vatsim_user['personal']['email'],
        'division'          : vatsim_user['vatsim']['division'],
        'region'            : vatsim_user['vatsim']['region'],
        'subdivision'       : vatsim_user['vatsim']['subdivision'],
        'pilot_ratings'     : _pluck_pilot_ratings(vatsim_user),
        'atc_rating'        : _pluck_atc_ratings_code(vatsim_user),
        'atc_rating_id'     : _pluck_atc_ratings_id(vatsim_user),
        'vacc_relation'     : _pluck_vacc_relation(vatsim_user),
        'country'           : vatsim_user['personal']['country'],
        'last_update'       : datetime.utcnow(),
    }

    user.update(vatsim_info)

def _pluck_pilot_ratings(vatsim_user):
    binary = f'1{format(vatsim_user["vatsim"]["pilotrating"]["id"], "b")}'
    ratings = [f'P{i}' for i in range(0, 8)]
    return [ratings[i] for i, rating in enumerate(binary) if int(rating) & 1]

def _pluck_atc_ratings_code(vatsim_user):
    return vatsim_user['vatsim']['rating']['short']

def _pluck_atc_ratings_id(vatsim_user):
    return vatsim_user['vatsim']['rating']['id']

def _pluck_vacc_relation(vatsim_user):
    inactive = vatsim_user['vatsim']['rating']['id'] == -1
    if inactive:
        return 'inactive'

    home = vatsim_user['vatsim']['subdivision']['id'] == 'POR'
    home_or_visiting = 'home' if home else 'visiting'

    pilot = vatsim_user['vatsim']['rating']['id'] == 1
    pilot_or_controller = 'pilot' if pilot else 'controller'

    return f'{home_or_visiting} {pilot_or_controller}'
