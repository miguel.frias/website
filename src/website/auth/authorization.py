def belongs_to_group(user, *groups):
    return set(user['groups']) & set(groups)

def update_groups(user, *groups):
    user['groups'] = list(groups)

def remove_groups(user, *groups):
    user['groups'] = list(set(user['groups']) - set(groups))
