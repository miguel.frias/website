import json
import requests
from functools import wraps
from flask import Blueprint, abort, url_for, redirect, render_template, request, current_app as app
from flask_login import UserMixin, current_user as user, login_user, logout_user, login_required

from . import authentication, authorization, vatsim, data
from .forms import EditGroupsForm

bp = Blueprint('auth', __name__, template_folder='templates')

required_scopes_error = '''
Revoke `VATSIM_PORTUGAL` access from `https://auth.vatsim.net/` then login \
again and grant all access from Portugal vACC organization.
'''.strip()

def add_cid_to_group(cid, *groups):
    user = data.get_user_for_cid(app.db, cid)
    authorization.update_groups(user, *groups)
    data.save(app.db, user)

def remove_cid_from_group(cid, *groups):
    user = data.get_user_for_cid(app.db, cid)
    authorization.remove_groups(user, *groups)
    data.save(app.db, user)

def group_required(*groups):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if authorization.belongs_to_group(user, *groups):
                return func(*args, **kwargs)
            _groups = ', '.join(groups)
            error   = f'User must belong to any of the following groups: {_groups}'
            abort(401, error)
        return wrapper
    return decorator

@app.template_filter('belongs_to_group')
def belongs_to_group_filter(user, *groups):
    return authorization.belongs_to_group(user, *groups)

@app.login_manager.user_loader
def load_user(user_id):
    return _User(data.get_user_for_cid(app.db, int(user_id)))

@app.login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('auth.login'))

@bp.route('/login')
def login():
    auth_host = app.config['SSO_API_URL']
    auth_url  = f'{auth_host}/login?callback={request.host_url}/callback'

    return redirect(auth_url, code=302)

@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))

@bp.route('/callback')
def callback():
    vatsim_auth = json.loads(request.args['result'])

    if not vatsim.has_required_scopes(vatsim_auth):
        view = {
            'error': required_scopes_error,
            'title': 'Required scopes not granted',
        }
        return render_template('public/error-page.html', **view)

    # get vatsim user
    auth_host = app.config['SSO_API_URL']
    auth_url  = vatsim.get_auth_url(vatsim_auth, auth_host)
    auth_resp = requests.get(auth_url)
    if not vatsim.is_valid_auth_response(auth_resp):
        abort(401, 'Verify your account with VATSIM.')

    # update our user data
    vastim_user = vatsim.get_vatsim_user(auth_resp)
    vastim_user['cid'] = int(vastim_user['cid'])
    if data.vatsim_user_exists(app.db, vastim_user):
        ## update user data
        user = data.get_user_for_vatsim_user(app.db, vastim_user)
        authentication.update_vatsim_info(user, vastim_user)
        data.save(app.db, user)
    else:
        ## create new user
        user = authentication.create_user(vatsim_user=vastim_user)
        user = data.add_user(app.db, user)

    login_user(_User(user))
    return redirect(url_for('dashboard'), code=302)

@bp.route('/users')
def list_users():
    users = data.get_users(app.db)

    users = [{**user, 'groups': ', '.join(user['groups'])} for user in users]

    return render_template('auth/list_users.html', users=users)

@bp.route('/groups/<user_id>', methods=['GET', 'POST'])
@group_required('admin')
def edit_groups(user_id):
    user = data.get_user_for_id(app.db, user_id)
    form = EditGroupsForm()

    if form.validate_on_submit():
        authorization.update_groups(user, *form.get_groups())
        data.save(app.db, user)

        return redirect(url_for('auth.list_users'))

    form.set_user(user)
    return render_template('auth/edit_groups.html', user=user, form=form)

class _User(dict, UserMixin):
    def __init__(self, other):
        super().__init__(other)

        ## this is what is passed to the load user callback
        ## by Flask-Login.
        ## see: https://flask-login.readthedocs.io/en/latest/#your-user-class
        self.id = other['cid']

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(f"'_User' object has no attribute '{key}'")
