def has_required_scopes(auth):
    required_scopes   = set(['full_name', 'vatsim_details', 'email', 'country'])
    authorized_scopes = set(auth['scopes'])

    return required_scopes.intersection(authorized_scopes) == required_scopes

def get_auth_url(vatsim_auth, auth_host):
    auth_host    = auth_host.strip('/')
    access_token = vatsim_auth['access_token']
    return f'{auth_host}/userinfo/{access_token}'

def is_valid_auth_response(auth_response):
    data             = auth_response.json()
    status_code_200  = auth_response.status_code == 200
    auth_token_valid = data['data']['oauth']['token_valid']

    return status_code_200 and auth_token_valid

def get_vatsim_user(auth_resp):
    return auth_resp.json()['data']
