import requests
import json

def get_airport_informations(icao):
    response = requests.get(
                f'https://beta-api.vatstats.net/external_api/airports/{icao}/?format=json'.format(
                    icao.upper()
                    )).content.decode('utf-8')
    return json.loads(response)
