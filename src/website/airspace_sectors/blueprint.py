import os
from flask import Blueprint, render_template, request, redirect, url_for, current_app as app
from functools import partial
from pymongo import MongoClient
from uuid import uuid4, UUID

from src.kernel.messages import MessageBuilder

from src.airspace_sectors_service import LPPC, LPPO
from src.airspace_sectors_service import RivCommandHandlers
from src.airspace_sectors_service import CriarSector, AlterarFrequenciaSector

from .model import ReadModel, ReadFacade

RivCommandHandlers.register(app.bus, app.store)
ReadModel.register(app.bus, app.db)
_read = ReadFacade(app.db)
bp = Blueprint('airspace_sectors', __name__, template_folder='templates')

@bp.route('/<riv_id>')
def riv(riv_id):
    model = _read.riv_view(UUID(riv_id))
    return render_template('sectors/riv.html', **model)

@bp.route('/<riv_id>/criar_sector', methods=['POST'])
def criar_sector(riv_id):
    payl = request.form

    callsign = f'{payl["callsign"].upper()}_{payl["posicao"]}'
    cmd = MessageBuilder() \
        .aggregate(UUID(riv_id), int(payl['riv_version'])) \
        .type(CriarSector) \
        .kwargs(
            sector_id=uuid4(),
            nome=payl['nome'].title(),
            callsign=callsign,
            posicao=payl['posicao'],
            frequencia=float(payl['frequencia']),
        ) \
        .build()
    app.bus.send(cmd)

    return redirect(url_for('airspace_sectors.riv', riv_id=riv_id))

@bp.route('/<riv_id>/sectors/<sector_id>/editar', methods=['POST'])
def editar_sector(riv_id, sector_id):
    rq = MessageBuilder() \
        .aggregate(
            UUID(riv_id),
            int(request.form['riv_version']),
        ) \
        .type(AlterarFrequenciaSector) \
        .args(UUID(sector_id), float(request.form['frequencia']))
    app.bus.send(rq.build())
    return redirect(url_for('airspace_sectors.riv', riv_id=riv_id))
