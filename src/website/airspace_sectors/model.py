from functools import singledispatchmethod
from uuid import UUID

from src.canonical_model.vatsim_ratings import PosicaoAtc
from src.airspace_sectors_service import SectorCriado, FrequenciaSectorAlterada

class RivView:
    def __init__(self, db):
        db.riv_view.drop()
        self.db = db.riv_view

    @singledispatchmethod
    def handle(self, evt): pass

    @handle.register
    def _(self, evt: SectorCriado):
        db = self.db
        lookup = {'riv.id': evt.aggregate_id}

        # lookup existing model
        model = db.find_one(lookup)
        if model is None:
            model = {
                'riv': {'id': evt.aggregate_id, 'version': evt.aggregate_version},
                'sectores': [],
            }

        # add sector
        model['riv']['version'] = evt.aggregate_version
        sector_model = {
            'id': evt.sector_id,
            'nome': evt.nome,
            'callsign': evt.callsign,
            'posicao': evt.posicao,
            'frequencia': evt.frequencia,
        }
        model['sectores'].append(sector_model)

        # save
        db.update_one(lookup, {'$set': model}, upsert=True)

    @handle.register
    def _(self, evt: FrequenciaSectorAlterada):
        db = self.db
        lookup = {'riv.id': evt.aggregate_id}

        # lookup existing model
        model = db.find_one(lookup)
        if model is None:
            model = {
                'riv': {'id': evt.aggregate_id, 'version': evt.aggregate_version},
                'sectores': [],
            }

        # update
        model['riv']['version'] = evt.aggregate_version
        for sector in model['sectores']:
            if sector['id'] == evt.sector_id:
                sector['frequencia'] = evt.frequencia

        # save
        db.update_one(lookup, {'$set': model}, upsert=True)

class ReadModel:
    @classmethod
    def register(cls, bus, db):
        bus.register(RivView(db).handle, SectorCriado, FrequenciaSectorAlterada)

class ReadFacade:
    def __init__(self, db):
        self.db = db

    def riv_view(self, riv_id: UUID):
        default = {'riv': {'id': riv_id, 'version': -1}}
        model = self.db.riv_view.find_one({'riv.id': riv_id}) or default
        posicoes = ['DEL', 'GND', 'TWR', 'APP', 'CTR', 'FSS']
        posicao_dto = lambda p: {'key': p, 'value': p}
        model['posicoes'] = tuple(map(posicao_dto, posicoes))
        return model
