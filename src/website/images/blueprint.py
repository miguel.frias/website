from flask import Blueprint, send_file, current_app as app
from .images import *

bp = Blueprint('images', __name__, url_prefix='/images')

@bp.route("/<string:iid>")
def show_image(iid):
    image = get(iid)
    response = send_file(image, attachment_filename='myfile.jpg', mimetype='image/png')
    return response


