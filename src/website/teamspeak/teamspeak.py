import ts3
import time
from dataclasses import dataclass
from abc import ABC
from enum import Enum
from ts3.query import TS3QueryError

class ServerGroups(Enum):
    S2 = 12
    S3 = 11
    C1 = 9
    C3 = 10
    I1 = 13
    I3 = 17
    SUP = 15
    P0 = 23
    P1 = 18
    P2 = 19
    P3 = 20
    P4 = 22
    Admin = 6
    Normal = 7

class TeamSpeak(ABC):
    def __init__(self, host, admin, passwd):
        _conn = ts3.query.TS3Connection(host)
        _conn.login(
            client_login_name = admin,
            client_login_password = passwd
        )
        _conn.use(sid=1)
        self.conn = _conn

@dataclass(frozen=True)
class Client:
    username: str
    cid: int
    atc_rating_code: str
    pilot_rating_code: str

@dataclass(frozen=True)
class FoundedClient(Client):
    clid: int
    cluid: str
    cldbid: str
    groups: list

    @property
    def new_groups(self):
        _groups = [
            ServerGroups.Normal.value,
            ServerGroups[self.pilot_rating_code].value]

        if self.atc_rating_code not in ['S1', 'OBS', 'INA']:
            _groups.append(ServerGroups[self.atc_rating_code].value)

        return _groups

class TeamSpeakService(TeamSpeak):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _get_client_cluid(self, clid):
        return self.conn.clientgetuidfromclid(clid=clid)[0]['cluid']

    def _get_client_cldbid(self, cluid):
        return self.conn.clientgetdbidfromuid(cluid=cluid)[0]['cldbid']

    def _get_client_groups(self, cldbid):
        return [int(server_group['sgid'])
            for server_group in self.conn.servergroupsbyclientid(cldbid=cldbid)]

    def find_client(self, client: Client):
        c = self.conn.clientfind(pattern=client.username)

        clid = int(c[0]['clid'])
        cluid = self._get_client_cluid(clid)
        cldbid = self._get_client_cldbid(cluid)
        groups = self._get_client_groups(cldbid)

        return FoundedClient(
            client.username,
            client.cid,
            client.atc_rating_code,
            client.pilot_rating_code,
            clid, cluid, cldbid, groups)

    def register_client(self, client: FoundedClient):
        #Save client discription
        self.conn.clientdbedit(cldbid=client.cldbid, client_description=client.cid)

        #Remove unused server groups
        for group in client.groups:
            try:
                if group != ServerGroups.Admin.value:
                    self.conn.servergroupdelclient(sgid=group, cldbid=client.cldbid)
                    time.sleep(0.5)
            except TS3QueryError:
                pass

        #Save groups
        for group in client.new_groups:
            self.conn.servergroupaddclient(sgid=group, cldbid=client.cldbid)
            time.sleep(0.5)

        if ServerGroups.Normal.value in client.new_groups:
            self.conn.clientkick(
                reasonid=5, reasonmsg="Registered! Welcome to Portugal-VACC", clid=client.clid)
