from flask import Blueprint, render_template, redirect, url_for
from flask_login import current_user as user, login_required
from os import environ

from .teamspeak import Client, TeamSpeakService
from ts3.query import TS3QueryError

bp = Blueprint('teamspeak', __name__, template_folder='templates')

@bp.route('/register', methods=['POST'])
@login_required
def register():
    title = 'Registration error'

    try:
        teamspeak = TeamSpeakService(
            environ.get('TS3_SERVER_HOST'),
            environ.get('TS3_SERVER_ADMIN'),
            environ.get('TS3_SERVER_ADMIN_PASS')
        )

        client = Client(
            user['name'][:30],
            user.cid,
            user['atc_rating'],
            user['pilot_ratings'][-1] if len(user.pilot_ratings) > 0 else 'P0')

        client = teamspeak.find_client(client)
        teamspeak.register_client(client)
    except TS3QueryError as crap:
        error = crap.resp.error['msg']
        return render_template('public/error-page.html', title=title, error=error)

    return redirect(url_for('dashboard'))
