<h1 id="airports">VFR Waypoints</h1>

<a type="button"
    href="https://drive.google.com/file/d/1Y6RffGRL6_Kp2FUDAblqlcROW6-BSShu/view?usp=sharing"
    name="button"
    class="btn">VFR Waypoints List</a>

<h1 id="airports">Airports</h1>

- [LPPR](#lppr)
- [LPPT](#lppt)
- [LPFR](#lpfr)
- [LPMA](#lpma)
- [LPPS](#lpps)

<h1 id="lppr">LPPR</h1>

### GENERAL

Runway 35 is the preferred runway in use by ATC due to taxiway layout.

### ARRIVAL

**Standard Approaches:**

-   RWY 35: RNP APCH
-   RWY 17: ILS APCH

**Speed Limitations:**

-   MAX IAS 280KT between FL245 and FL100;
-   MAX IAS 250KT at and below FL100;
-   MAX IAS 220KT at and below FL70;
-   MAX IAS 200KT at and below 4000ft;
-   MAX IAS between 180KT and 160KT when established on final APCH segment;
-   Thereafter 160KT until 4NM from THR.

**RWY 35:**

-   ATC expects arriving aircraft to vacate on taxiway F. Advise ATC if unable. 180º turns on the runway are allowed. There is a risk of go around if preceding traffic overshoots taxiway F.

**RWY 17:**

-   Do not vacate RWY via TWY F or A3 unless cleared by ATC.

### DEPARTURE

**Clearance:**

Contact DLV with the following information:
- Callsign;
- Stand;
- ATIS code.

**Intersection TKOF:**

- Advise ATC ASAP if able for departure from a RWY intersection (A3 or F for RWY 17; C, D, H or J for RWY 35).

**Initial climb and initial contact:**

- ALL DEPARTURES are to climb to FL100. Do not climb above FL100 until instructed to do so;
- After take-off contact Porto APP, do not wait for a handoff from TWR.

**NADP:**

- NADP PROC B in use at Portugal.

[Go to top](#airports)

<h1 id="lppt">LPPT</h1>

### GENERAL

Runway 03 is the preferred runway in use by ATC due to taxiway layout;
Runway 17/35 is closed and renamed as taxiway T;
Taxiway R1, R2 and S1 renamed as taxiway A;
Squawk mode C on the ground.

### ARRIVAL

**Standard Approaches:**

- RWY 03: ILS APCH
- RWY 21: ILS APCH


**Speed Limitations:**

-   MAX IAS 280KT between FL245 and FL100;
-   MAX IAS 250KT at and below FL100;
-   MAX IAS 220KT at and below FL70;
-   MAX IAS 200KT at and below 4000ft;
-   MAX IAS between 180KT and 160KT when established on final APCH segment;
-   Thereafter 160KT until 4NM from THR.

**Communications:**

- On first contact with APP report callsign, cleared LVL and STAR.


**RWY 03:**

- ATC expects arriving aircraft to vacate via HN;
- If pilots wish to vacate via TWY A6 or TWY T, make the REQ in the first contact with TWR.


**RWY 21:**

- ATC expects arriving aircraft to vacate via HS;
- If pilots wish to vacate BEFORE HS, make the REQ in the first contact with TWR.


### DEPARTURE

**Clearance:**

Contact DLV with the following information:
- Callsign;
- Stand;
- ATIS code;
- Destination.

**Intersection TKOF:**

- When RWY 03 in use, ACFT taxiing via G2, U1 and N1 expect position N for departure. ACFT taxiing via M4 expect position M for departure. If unable advise before starting taxi;
- When RWY 21 is in use, the preferred departure position for all ACFT, except HEAVY Jets, should be position U.


**Initial climb and initial contact:**

- ALL DEPARTURES are to climb to FL060. Do not climb above FL060 until instructed to do so;
- After take-off contact Lisboa APP, do not wait for a handoff from TWR.

**NADP:**

- NADP PROC B in use at Portugal.

[Go to top](#airports)

<h1 id="lpfr">LPFR</h1>

### GENERAL

Runway 28 is the usual runway in use.

### ARRIVAL

**Standard Approaches:**

- RWY 28: ILS-Z APCH
- RWY 10: ILS-Z APCH


**Speed Limitations:**

-   MAX IAS 280KT between FL245 and FL100;
-   MAX IAS 250KT at and below FL100;
-   MAX IAS 220KT at and below FL70;
-   MAX IAS 200KT at and below 4000ft;
-   MAX IAS between 180KT and 160KT when established on final APCH segment;
-   Thereafter 160KT until 4NM from THR.

### DEPARTURE

**Clearance:**

Contact DLV with the following information:
- Callsign;
- Stand;
- Cruising Level;
- ATIS code.

**Initial climb and initial contact:**

- ALL DEPARTURES are to climb to FL060. Do not climb above FL060 until instructed to do so;
- After take-off contact Faro APP, do not wait for a handoff from TWR.

**NADP:**

- NADP PROC B in use at Portugal.

[Go to top](#airports)

<h1 id="lpma">LPMA</h1>

### GENERAL

- Runway 05 is the usual runway in use. It also is the most know approach to Madeira;
- Unexpected wind variations, turbulence, updrafts, downdrafts and low altitude windshear shall be anticipated when operating at LPMA. These weather phenomena can also occur with relatively calm wind speeds;
- Delays may be expected due to conflicting approach paths/missed approach procedures with departure procedures;
- 180° turn for ACFT with MTOW 30t / 66139lbs or above at turning bays only.


### ARRIVAL

**Standard Approaches:**

- RWY 05: VOR DME APCH
- RWY 23: VOR DME APCH
If you want other approaches (RNP, etc.), request it to the ATC before executing it.


**Speed Limitations:**

-   MAX IAS 280KT between FL245 and FL100;
-   MAX IAS 250KT at and below FL100;
-   MAX IAS 220KT at and below FL70;
-   MAX IAS 200KT at and below 4000ft;


### DEPARTURE

**Clearance:**

Contact DLV with the following information:
- Callsign;
- Stand;
- Cruising Level;
- ATIS code.

**Initial climb and initial contact:**

- ALL DEPARTURES (except NIDUL DEPs) are to climb to FL060. Do not climb above FL060 until instructed to do so;
- After take-off contact Madeira APP, do not wait for a handoff from TWR.


**NADP:**

- NADP PROC B in use at Portugal.

[Go to top](#airports)

<h1 id="lpps">LPPS</h1>

### GENERAL

- Runway 36 is the usual runway in use;
- 180° turn for ACFT with MTOW 30t / 66139lbs or above at turning bays only.


### ARRIVAL

**Standard Approaches:**

- RWY 36: VOR APCH
- RWY 18: VOR APCH

**Speed Limitations:**

-   MAX IAS 280KT between FL245 and FL100;
-   MAX IAS 250KT at and below FL100;
-   MAX IAS 220KT at and below FL70;
-   MAX IAS 200KT at and below 4000ft;
-   MAX IAS between 180KT and 160KT when established on final APCH segment;
-   Thereafter 160KT until 4NM from THR.

### DEPARTURE

**Clearance:**

Contact DLV with the following information:
- Callsign;
- Stand;
- Cruising Level;
- ATIS code.

**Initial climb and initial contact:**

- ALL DEPARTURES are to climb to FL060. Do not climb above FL060 until instructed to do so;
- After take-off contact Madeira APP, do not wait for a handoff from TWR.


**NADP:**

- NADP PROC B in use at Portugal.

[Go to top](#airports)
