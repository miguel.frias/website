# Oceanic Clearance

Request Oceanic Clearance 40 minutes before entering Santa Maria airspace.

Oceanic Clearance can be requested via voice procedures or datalink:

## Datalink Oceanic Clearance Tutorial

<iframe
    width="560"
    height="315"
    src="https://www.youtube.com/embed/l_qZCd5GDYU"
    frameborder="0"
    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen>
</iframe>

## Voice Procedures

The clearance request should contain the following:

- The Oceanic Entry Point and the estimated time
- The requested Mach number and flight level at the Oceanic Entry Point
- The highest acceptable flight level which can be maintained at the OCA entry
- Other information deemed important by the PILOT

After the clearance a SELCAL check should be requested.

# CPDLC

Planes equipped with CPDLC do not need position reports when logged on to LPPO before the entry point.

# Position Reports

The position report should contain the following:

- The current waypoint and time over this waypoint
- The current FL
- The next waypoint and time estimate to the next waypoint
- The waypoint after the next
- Other information deemed important by the PILOT

Flying westbound should be an even FL/ Eastbound odd FL.

# Downloads (Public)

<a type="button"
    href="https://www.hoppie.nl/acars/prg/air/"
    name="button"
    class="btn">Hoppie CPDLC</a>

# Documentation

<a type="button"
    href="https://drive.google.com/file/d/1_je2CBw_z_-qYo3OxChVLOxTvV-01WbA/view?usp=sharing"
    name="button"
    class="btn">LPPO Pilot Guide</a>
