from bson.objectid import ObjectId
from flask import current_app as app
from ..auth import get_user_for_cid

def add_session(db, session):
    return db['atcsessions'].insert_one(session)

def can_add_session(db, session):
    _live_sessions = find_atcsessions_by_predicate(
        db, position=session['position'], type='Live')
    _training_sessions = find_atcsessions_by_predicate(
        db, position=session['position'], type='Training')
    sweatbox_sessions = find_atcsessions_by_predicate(
        db, position=session['position'], type='Sweatbox')
    other_sessions = list(_live_sessions) + list(_training_sessions)
    for _session in sweatbox_sessions if session['type'] == 'Sweatbox' else other_sessions:
        if _session['starts'] <= session['starts'] < _session['ends']:
            return False
        elif _session['starts'] < session['ends'] <= _session['ends']:
            return False
        elif session['starts'] <= _session['starts'] < session['ends']:
            return False
        elif session['starts'] <= _session['ends'] < session['ends']:
            return False
    return True

def find_atcsessions_by_predicate(db, **predicate):
    return db['atcsessions'].find(predicate)

def get_atcsessions(db):
    atcsessions = list(db['atcsessions'].find())
    ## handle database inconsistency
    for session in atcsessions:
        user = get_user_for_cid(app.db, int(session['controller']))
        mentor = get_user_for_cid(app.db, session['mentor'])
        session['controller_name'] = user['name']
        session['mentor_name'] = ''
        if mentor:
            session['mentor_name'] = mentor['name']
        yield session

def get_atcsessions_from_user(db, user_cid):
    atcsessions = db['atcsessions'].find({'controller':user_cid})
    ## handle database inconsistency
    for session in atcsessions:
        user = get_user_for_cid(app.db, int(session['controller']))
        mentor = get_user_for_cid(app.db, session['mentor'])
        session['controller_name'] = user['name']
        session['mentor_name'] = ''
        if mentor:
            session['mentor_name'] = mentor['name']
        yield session

def get_atcpositions_until_rating(db, rating):
    result = []
    positions = db['atcpositions'].find()
    for position in positions:
        if position['min_rating'] <= rating:
            result += [position]
    return result

def get_atcpositions_not_granted(db, userid):
    user = get_user_for_cid(app.db, userid)
    _positions = get_training_atcpositions_until_rating(db, user['vatsim']['rating']['id'])
    for position in _positions:
        if position['position'] not in [cert['certification'] for cert in user['atc_certifications']]:
            yield position

def get_atcpositions(db):
    return db['atcpositions'].find()

def get_training_atcpositions_until_rating(db, rating):
    return get_atcpositions_until_rating(db, rating + 1)

def get_trainee_mentors(db, trainee_cid):
    mentors = list(db['atcmentors'].find())
    return [
        mentor for mentor in mentors
        if trainee_cid in
        [trainee['cid'] for trainee in mentor['trainees']]
    ]

def remove_atcsession(db, _id):
    return db['atcsessions'].delete_one({'_id': ObjectId(_id)})

def save_atcposition(db, atcposition):
    lookup = {'position': atcposition['position']}
    position = db['atcpositions'].find_one(lookup)
    if not position:
        return db['atcpositions'].insert_one(atcposition)
    return db['atcpositions'].update_one(lookup, {'$set': atcposition})
