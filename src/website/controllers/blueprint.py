import json
import requests
from flask import Blueprint, render_template, request, url_for, redirect, current_app as app
from flask_login import current_user, login_required

from server.entities.atcratings import ATCRating

from ..atcmentoring import find_mentor_by_trainee
from ..auth import group_required, get_user_for_cid, save_user

from server.entities.atcratings import ATCRating

from . import entities
from . import data

bp = Blueprint('controllers', __name__, template_folder='templates', url_prefix='/controllers')

@bp.route("/scheduling")
def scheduling():
    atcsessions = list(data.get_atcsessions(app.db))
    for i, session in enumerate(atcsessions):
        atcsessions[i] = entities.to_calendar_format(session)
    return render_template(
        "controllers/scheduling.html",
        scheduling=atcsessions)

@bp.route("/sessions/delete", methods=["POST"])
@login_required
def atcsessionsdeletepage():
    data.remove_atcsession(app.db, request.form['sessionid'])
    return redirect(url_for('dashboard'))

@bp.route("/sessions/new", methods=["GET", "POST"])
@login_required
def newsession():
    #TODO:
    #Use flask_forms to remove this
    def valid_form(form):
        if form['startsdate'] == '':
            raise Exception("Starts date in fault!")
        elif form['endsdate'] == '':
            raise Exception("Ends date in fault!")
        elif form['startstime'] == '':
            raise Exception("Starts time in fault!")
        elif form['endstime'] == '':
            raise Exception("Ends time in fault!")
    mentors = data.get_trainee_mentors(app.db, current_user.cid)
    atcpositions = data.get_atcpositions_until_rating(
        app.db, current_user['atc_rating_id'])
    training_atcpositions =  data.get_training_atcpositions_until_rating(
        app.db, current_user['atc_rating_id'])
    view = { 'error':None, 'atcpositions':atcpositions,
        'training_atcpositions':training_atcpositions, 'mentors':mentors }
    if request.method == 'POST':
        try:
            valid_form(request.form)
            _position = entities.get_user_position_by_type(
                request.form['type'],
                request.form['position'],
                request.form['training_position'],
            )
            session = entities.create_atcsession(
                int(request.form['cid']),
                request.form['type'],
                _position,
                request.form['startsdate'], request.form['endsdate'],
                request.form['startstime'], request.form['endstime'],
                request.form['mentor'] if 'mentor' in request.form else None,
            )
            if data.can_add_session(app.db, session):
                data.add_session(app.db, session)
                return redirect(url_for('controllers.scheduling'))
            view['error'] = 'Other session already booked on this time.'
        except Exception as ex:
            view['error'] = ex
    return render_template("controllers/new_session.html", **view)

@bp.route("/positions")
@login_required
def positions():
    _atcpositions = data.get_atcpositions(app.db)
    view = {'atcpositions': _atcpositions, 'atcratings':ATCRating}
    return render_template("controllers/positions.html", **view)

@bp.route("/positions/set", methods=["GET", "POST"])
@login_required
@group_required('admin', 'atcpositions_editor')
def setposition():
    if request.method == 'POST':
        _position = entities.create_atcposition(
            request.form["position"],
            request.form["identifier"],
            request.form['frequency'],
            request.form['limit']
        )
        data.save_atcposition(app.db, _position)
        return redirect(url_for('controllers.positions'))
    return render_template('controllers/set_position.html')

@bp.route("/<int:cid>/certifications")
@login_required
@group_required('admin', 'training_director')
def usercertifications(cid):
    user = get_user_for_cid(app.db, cid)
    view = {'user': user}
    return render_template("controllers/user_certifications.html", **view)

@bp.route("/<int:cid>/certifications/add", methods=["GET", "POST"])
@login_required
@group_required('admin', 'training_director')
def add_certification(cid):
    if request.method == 'POST':
        _user = get_user_for_cid(app.db, cid)
        _certification = entities.create_certification(
            cid, request.form['certification'], current_user.cid)
        entities.add_certification(_user, _certification)
        save_user(app.db, _user)
        return redirect(url_for('controllers.usercertifications', cid=cid))
    user = get_user_for_cid(app.db, cid)
    _atcpositions = data.get_atcpositions_until_rating(app.db, user['vatsim']['rating']['id'])
    non_granted_positions = data.get_atcpositions_not_granted(app.db, cid)
    view = {'user': user, 'positions':non_granted_positions}
    return render_template("controllers/add_certification.html", **view)

@bp.route("/<int:cid>/certifications/delete", methods=["POST"])
@login_required
@group_required('admin', 'training_director')
def delete_certification(cid):
    _user = get_user_for_cid(app.db, cid)
    _icao = request.form['certification']
    entities.remove_certification(_user, _icao)
    save_user(app.db, _user)
    return redirect(url_for('controllers.usercertifications', cid=cid))
