from ..auth import get_users
from flask import current_app as app

def can_add_mentor(db, mentor):
    return db['atcmentors'].find_one({'cid': mentor['cid']}) is None

def add_mentor(db, mentor):
    return db['atcmentors'].insert_one(mentor)

def get_mentors(db):
    return db['atcmentors'].find()

def get_mentor_for_cid(db, cid):
    return db['atcmentors'].find_one({'cid': cid})

def get_non_mentors(db):
    users       = list(get_users(app.db))
    mentors     = db['atcmentors'].find()
    mentor_cids = set([mentor['cid'] for mentor in mentors])

    ## handle database inconsistency
    def add_name(user):
        return {**user, 'name': user['name']}

    return [add_name(user) for user in users if user['cid'] not in mentor_cids]

def get_non_trainees(db, mentor):
    users        = list(get_users(app.db))
    trainee_cids = set([trainee['cid'] for trainee in mentor['trainees']])

    return [user for user in users if user['cid'] not in trainee_cids]

def save_mentor(db, mentor):
    return db['atcmentors'].update_one({'cid': mentor['cid']}, {'$set': mentor})

def remove_mentor_for_cid(db, cid):
    return db['atcmentors'].delete_one({'cid': cid})
