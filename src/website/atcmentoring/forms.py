from flask_wtf import FlaskForm
from wtforms import SelectField, BooleanField, HiddenField, DateField, SubmitField
from wtforms.validators import DataRequired
import datetime

_facilities = [
    (2, "DEL/GND"),
    (3, "TWR"),
    (4, "APP"),
    (5, "CTR"),
    (7, "FSS"),
]

atsimtest_states = [
    ("not-assigned", "Not Assigned"),
    ("assigned", "Assigned"),
    ("complete", "Complete")
]

def _get_facilities_until_atcrating(limit):
    facilities = {x[0]:x[1] for x in _facilities}
    return [(key, value) for key, value in facilities.items() if key <= limit]


class NewMentorForm(FlaskForm):
    cid = SelectField('cid', coerce=int, validators=[DataRequired()])
    limit = SelectField('limit', coerce=int, validators=[DataRequired()], choices=_facilities)
    fir_lppo = BooleanField('LPPO')
    fir_lppc = BooleanField('LPPC')
    tma_lppt = BooleanField('LPPT')
    tma_lppr = BooleanField('LPPR')
    tma_lpfr = BooleanField('LPFR')
    tma_lpma = BooleanField('LPMA')
    tma_lppd = BooleanField('LPPD')
    tma_lpla = BooleanField('LPLA')
    tma_lphr = BooleanField('LPHR')
    submit   = SubmitField('Add New Mentor')

    def __init__(self, non_mentors):
        super().__init__()

        self.cid.choices = [(m['cid'], m['name']) for m in non_mentors]

class EditMentorForm(FlaskForm):
    cid = HiddenField()
    limit = SelectField('limit', coerce=int, validators=[DataRequired()], choices=_facilities)
    fir_lppo = BooleanField('LPPO')
    fir_lppc = BooleanField('LPPC')
    tma_lppt = BooleanField('LPPT')
    tma_lppr = BooleanField('LPPR')
    tma_lpfr = BooleanField('LPFR')
    tma_lpma = BooleanField('LPMA')
    tma_lppd = BooleanField('LPPD')
    tma_lpla = BooleanField('LPLA')
    tma_lphr = BooleanField('LPHR')
    submit   = SubmitField('Save')

    def __init__(self, cid):
        super().__init__()

        self.cid.data = cid

class NewTraineeForm(FlaskForm):
    mentorid = HiddenField()
    trainieid = SelectField('trainieid', coerce=int, validators=[DataRequired()])
    facility = SelectField('facility', coerce=int, validators=[DataRequired()])
    atsimtest = SelectField('atsimtest', validators=[DataRequired()], choices=atsimtest_states)
    atsimtest_validity = DateField(format='%d/%m/%Y', default=datetime.date.today)
    submit   = SubmitField('Add Trainee')

    def __init__(self, non_trainees, mentor):
        super().__init__()
        _choises = []
        for non_trainee in non_trainees:
            key   = non_trainee['cid']
            value = non_trainee['name']
            _choises.append((key, value))
        self.trainieid.choices = _choises
        self.facility.choices = _get_facilities_until_atcrating(mentor['limit'])

class EditTraineeForm(FlaskForm):
    mentorid = HiddenField()
    trainieid = HiddenField()
    facility = SelectField('facility', coerce=int, validators=[DataRequired()])
    atsimtest = SelectField('atsimtest', validators=[DataRequired()], choices=atsimtest_states)
    atsimtest_validity = DateField(format='%d/%m/%Y', default=datetime.date.today)
    submit   = SubmitField('Save')

    def __init__(self, cid, mentor):
        super().__init__()

        self.trainieid.data = cid
        self.mentorid.data = mentor['cid']
        self.facility.choices = _get_facilities_until_atcrating(mentor['limit'])
