from datetime import datetime

def create_mentor(user, limit, firs, tmas):
    return {
        'cid'     : user['cid'],
        'name'    : user['name'],
        'limit'   : limit,
        'firs'    : [fir.upper() for fir in firs],
        'tmas'    : [tma.upper() for tma in tmas],
        'trainees': [],
    }

def update_mentor(mentor, limit, firs, tmas):
    mentor['limit'] = limit
    mentor['firs'] = firs
    mentor['tmas'] = tmas

def create_trainee(user, facility, firs, tmas, atsimtest_status, atsimtest_validity):
    return {
        'cid': user['cid'],
        'name':user['name'],
        'facility': facility,
        'firs'    : [fir.upper() for fir in firs],
        'tmas'    : [tma.upper() for tma in tmas],
        'atsimtest_status':atsimtest_status,
        'atsimtest_validity': atsimtest_validity
    }

def add_trainee(mentor, trainee):
    mentor['trainees'].append(trainee)

def get_trainee_from_cid(mentor, traineeid):
    for trainee in mentor['trainees']:
        if trainee['cid'] == traineeid:
            return trainee

def update_trainee(mentor, trainee, **changes):
    keys = set(['facility', 'tmas', 'firs', 'atsimtest_status', 'atsimtest_validity'])
    for i, _trainee in enumerate(mentor['trainees']):
        if _trainee['cid'] == trainee['cid']:
            _changes = {key:value for key, value in changes.items() if key in keys}
            mentor['trainees'][i].update(_changes)

def remove_trainee(mentor, trainee):
    for i, _trainee in enumerate(mentor['trainees']):
        if _trainee['cid'] == trainee['cid']:
            mentor['trainees'].pop(i)
