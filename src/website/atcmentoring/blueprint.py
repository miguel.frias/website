import json
from datetime import datetime
from flask import Blueprint, redirect, render_template, request, url_for, current_app as app
from flask_login import current_user as user, login_required, login_manager

from .atcmentoring import get_facility, get_avail_facilities
from .forms import NewMentorForm, EditMentorForm, NewTraineeForm, EditTraineeForm

from . import entities
from . import data
from ..auth import group_required, add_cid_to_group, remove_cid_from_group, get_user_for_cid

bp = Blueprint('atcmentoring', __name__, template_folder='templates', url_prefix='/atcmentoring')

@bp.route('/mentors/new', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'training_director', 'atcmentor_editor')
def newmentor():
    non_mentors = data.get_non_mentors(app.db)

    form = NewMentorForm(non_mentors)
    if form.validate_on_submit():
        mentor = {
            'user' : get_user_for_cid(app.db, int(request.form['cid'])),
            'limit': int(request.form['limit']),
            'firs' : [key[4:].upper() for key, value in request.form.items() if 'fir_' in key and value == 'y'],
            'tmas' : [key[4:].upper() for key, value in request.form.items() if 'tma_' in key and value == 'y'],
        }
        mentor = entities.create_mentor(**mentor)
        data.add_mentor(app.db, mentor)
        add_cid_to_group(mentor['cid'], 'mentor')

        return redirect(url_for('atcmentoring.mentors_dashboard'))

    non_mentors = data.get_non_mentors(app.db)
    return render_template('atcmentoring/new_mentor.html', users=non_mentors, form=form)

@bp.route('/')
@login_required
@group_required('admin', 'training_director', 'mentor', 'atcmentor_editor')
def mentors_dashboard():
    mentors = list(data.get_mentors(app.db))

    view = {'mentors': mentors}
    return render_template('atcmentoring/mentors_dashboard.html', **view)

@bp.route("/mentors")
def mentors_public():
    mentors = data.get_mentors(app.db)

    return render_template('atcmentoring/mentors_public.html', mentors=mentors)

@bp.route('/mentors/<int:mentorid>/edit', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'training_director', 'atcmentor_editor')
def editmentor(mentorid):
    mentor = data.get_mentor_for_cid(app.db, mentorid)

    form = EditMentorForm(mentorid)
    if form.validate_on_submit():
        if mentor:
            changes = {
                'limit': int(request.form['limit']),
                'firs' : [key[4:].upper() for key, value in request.form.items() if 'fir_' in key and value == 'y'],
                'tmas' : [key[4:].upper() for key, value in request.form.items() if 'tma_' in key and value == 'y'],
            }
            entities.update_mentor(mentor, **changes)
            data.save_mentor(app.db, mentor)
        return redirect(url_for('atcmentoring.mentors_dashboard'))

    view = {'mentor': mentor, 'mentor_json': json.loads(json.dumps(mentor, default=lambda y: y.__str__())), 'form':form}
    return render_template('atcmentoring/edit_mentor.html', **view)

@bp.route('/mentors/delete', methods=['POST'])
@login_required
@group_required('admin', 'training_director', 'atcmentor_editor')
def deletementor():
    mentor_cid = int(request.form['mentorid'])
    data.remove_mentor_for_cid(app.db, mentor_cid)
    remove_cid_from_group(mentor_cid, 'mentor')

    return redirect(url_for('atcmentoring.mentors_dashboard'))

@bp.route('/mentors/<int:mentorid>/trainees/new', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'training_director', 'atcmentor_editor')
def newtrainee(mentorid):
    mentor = data.get_mentor_for_cid(app.db, mentorid)
    non_trainees = data.get_non_trainees(app.db, mentor)
    form = NewTraineeForm(non_trainees, mentor)
    if form.validate_on_submit():
        _at_val = request.form['atsimtest_validity']
        _at_val = datetime.strptime(_at_val, r'%d/%m/%Y') if _at_val else ''
        trainee = {
            'user'              : get_user_for_cid(app.db, int(request.form['trainieid'])),
            'facility'          : int(request.form['facility']),
            'firs'              : [key[4:].upper() for key, value in request.form.items() if 'fir_' in key and value == 'y'],
            'tmas'              : [key[4:].upper() for key, value in request.form.items() if 'tma_' in key and value == 'y'],
            'atsimtest_status'  : request.form['atsimtest'],
            'atsimtest_validity': _at_val,
        }
        trainee = entities.create_trainee(**trainee)
        entities.add_trainee(mentor, trainee)
        data.save_mentor(app.db, mentor)
        add_cid_to_group(trainee['cid'], 'trainee')
        return redirect(url_for('atcmentoring.trainees', mentorid=mentorid))

    view = {'mentor': mentor, 'form':form}
    return render_template('atcmentoring/new_trainee.html', **view)

@bp.route('/')
@bp.route('/mentors/<int:mentorid>/trainees')
@login_required
@group_required('admin', 'training_director', 'mentor', 'atcmentor_editor')
def trainees(mentorid=None):
    cid = mentorid if mentorid else user.cid
    mentor = data.get_mentor_for_cid(app.db, cid)

    for trainee in mentor['trainees']:
        _at_val = 'atsimtest_validity'
        if trainee[_at_val]:
            trainee[_at_val] = trainee[_at_val].strftime(r'%Y-%m-%d')

    view = {'mentor': mentor, 'get_facility': get_facility}
    return render_template('atcmentoring/trainees.html', **view)

@bp.route('/mentors/<int:mentorid>/trainees/<int:traineeid>/edit', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'training_director', 'atcmentor_editor')
def edittrainee(mentorid, traineeid):
    mentor = data.get_mentor_for_cid(app.db, mentorid)
    trainee = entities.get_trainee_from_cid(mentor, traineeid)
    form = EditTraineeForm(traineeid, mentor)
    if form.validate_on_submit():
        _at_val = request.form['atsimtest_validity']
        _at_val = datetime.strptime(_at_val, r'%d/%m/%Y') if _at_val else ''
        changes = {
            'firs'              : [key[4:].upper() for key, value in request.form.items() if 'fir_' in key and value == 'on'],
            'tmas'              : [key[4:].upper() for key, value in request.form.items() if 'tma_' in key and value == 'on'],
            'facility'          : int(request.form['facility']),
            'atsimtest_status'  : request.form['atsimtest'],
            'atsimtest_validity': _at_val,
        }
        entities.update_trainee(mentor, trainee, **changes)
        data.save_mentor(app.db, mentor)
        return redirect(url_for('atcmentoring.trainees', mentorid=mentorid))

    view = {'mentor': mentor, 'trainee': trainee, 'form':form}
    return render_template('atcmentoring/edit_trainee.html', **view)

@bp.route('/mentors/<int:mentorid>/trainees/delete', methods=['POST'])
@login_required
@group_required('admin', 'training_director', 'atcmentor_editor')
def delete_trainee(mentorid):
    mentor = data.get_mentor_for_cid(app.db, mentorid)
    trainee_cid = int(request.form['trainieid'])
    trainee = entities.get_trainee_from_cid(mentor, trainee_cid)

    entities.remove_trainee(mentor, trainee)
    data.save_mentor(app.db, mentor)
    remove_cid_from_group(trainee['cid'], 'trainee')

    return redirect(url_for('atcmentoring.trainees', mentorid=mentorid))

