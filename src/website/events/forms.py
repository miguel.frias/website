from flask_wtf import FlaskForm
from wtforms import DateTimeField, StringField, FileField, SubmitField
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms.validators import InputRequired
from wtforms.widgets import TextArea

from datetime import datetime

class NovoEventoForm(FlaskForm):
    titulo = StringField('Title', validators=[InputRequired()])
    inicio = DateTimeField('Start', validators=[InputRequired()],
                                format = "%Y-%m-%dT%H:%M", default=datetime.utcnow)
    fim = DateTimeField('End', validators=[InputRequired()],
                                format = "%Y-%m-%dT%H:%M", default=datetime.utcnow)
    banner = FileField('Banner', validators=[FileRequired(),
                                            FileAllowed(['jpg', 'png'], 'Images only!')])
    descricao = StringField('Description', widget=TextArea(), validators=[InputRequired()])

    submit  = SubmitField('Create Event')

class EditaEventoForm(FlaskForm):
    titulo = StringField('Title', validators=[InputRequired()])
    descricao = StringField('Description', widget=TextArea(), validators=[InputRequired()])

    submit  = SubmitField('Edit Event')
