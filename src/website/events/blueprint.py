import os
from flask import render_template, Blueprint, request, redirect, url_for, current_app as app
from flask_login import login_required, current_user
from pymongo import MongoClient
from ..auth import group_required, belongs_to_group
from ..images import save_image, get as get_image

from uuid import uuid4, UUID
from datetime import datetime
from src.eventos_service import EventoCommandHandlers, EventoReadModel, EventoReadFacade
from src.eventos_service import CriarEvento, DefinirParametroEvento, PublicarEvento
from src.kernel.messages import MessageBuilder

from .forms import NovoEventoForm, EditaEventoForm

TIME_FORMAT = r'%Y-%m-%dT%H:%M'

db = MongoClient(os.environ.get('MONGO_URI2'))
EventoCommandHandlers.register(app.bus, app.store)
EventoReadModel.register(app.bus, db.eventos)
read = EventoReadFacade(db.eventos)
bp = Blueprint('events', __name__, template_folder='templates', url_prefix='/events')

@bp.route('/')
def eventslist():
    user = None if current_user.is_anonymous else UUID(int=current_user.id)
    eventos = read.eventos(criado_por=user, publicado=True)
    return render_template('events/list.html', eventos=eventos)

@bp.route('/manage')
@login_required
@group_required('admin', 'events_editor')
def dashboard():
    eventos = read.eventos(criado_por=UUID(int=current_user.id))
    return render_template('events/manage.html', eventos=eventos)

@bp.route('/new', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'events_editor')
def new():
    form = NovoEventoForm()
    if form.validate_on_submit():
        model = request.form
        evento = uuid4()
        criado_por = UUID(int=current_user.id)
        inicio = datetime.strptime(model['inicio'], TIME_FORMAT)
        fim = datetime.strptime(model['fim'], TIME_FORMAT)

        titulo = model['titulo']
        descricao = model['descricao']
        banner = save_image(form.banner.data)

        criar = MessageBuilder() \
            .aggregate(evento, -1) \
            .type(CriarEvento) \
            .args(criado_por, inicio, fim)
        titulo = MessageBuilder() \
            .aggregate(evento, 0) \
            .type(DefinirParametroEvento) \
            .args('titulo', titulo)
        descricao = MessageBuilder() \
            .aggregate(evento, 1) \
            .type(DefinirParametroEvento) \
            .args('descricao', descricao)
        banner = MessageBuilder() \
            .aggregate(evento, 2) \
            .type(DefinirParametroEvento) \
            .args('banner', banner)
        app.bus.send(criar.build())
        app.bus.send(titulo.build())
        app.bus.send(descricao.build())
        app.bus.send(banner.build())

        return redirect(url_for('events.dashboard'))
    return render_template('events/new.html', form=form)

@bp.route('<evento_id>/<version>/publish', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'events_editor')
def publish(evento_id, version):
    rq = MessageBuilder() \
            .aggregate(UUID(evento_id), int(version)) \
            .type(PublicarEvento)
    app.bus.send(rq.build())

    return redirect(url_for('events.dashboard'))

@bp.route('<evento_id>/<version>/edit', methods=['GET', 'POST'])
@login_required
@group_required('admin', 'events_editor')
def edit(evento_id, version):
    form = EditaEventoForm()

    evento = UUID(evento_id)
    args = read.evento(evento)

    form.titulo.data = args['titulo']
    form.descricao.data = args['descricao']

    if form.validate_on_submit():
        titulo = MessageBuilder() \
            .aggregate(evento, int(version)) \
            .type(DefinirParametroEvento) \
            .args('titulo', request.form['titulo'])
        descricao = MessageBuilder() \
            .aggregate(evento, (int(version) + 1)) \
            .type(DefinirParametroEvento) \
            .args('descricao', request.form['descricao'])

        app.bus.send(titulo.build())
        app.bus.send(descricao.build())
        return redirect(url_for('events.dashboard'))
    return render_template('events/edit.html', form=form)
