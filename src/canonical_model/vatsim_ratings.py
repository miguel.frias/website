from enum import Enum

class Rating(int, Enum):
    '''O Rating da VATSIM.'''
    INA = -1
    OBS = 1
    S1 = 2
    S2 = 3
    S3 = 4
    C1 = 5
    C3 = 7
    I1 = 8
    I3 = 10
    SUP = 11

class PosicaoAtc(int, Enum):
    '''Classificacao de tipos de posicoes de ATC da VATSIM.'''
    DEL = Rating.S1
    GND = Rating.S1
    TWR = Rating.S2
    APP = Rating.S3
    CTR = Rating.C1
    FSS = Rating.C1
