from .posicao_aggregate import PosicaoCommandHandlers
from .posicao_aggregate import (
    MarcarSessao,
    CancelarSessao
)
from .posicao_aggregate import (
    SessaoMarcada,
    SessaoCancelada,
)

