from .domain import Posicao
from .domain import MarcarSessao, CancelarSessao
from .domain import SessaoMarcada, SessaoCancelada
from .commands import PosicaoCommandHandlers
