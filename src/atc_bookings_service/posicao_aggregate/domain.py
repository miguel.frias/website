from datetime import datetime
from dataclasses import dataclass
from functools import singledispatchmethod
from src.kernel.messages import MessageBuilder, command, event
from src.kernel.aggregate_root import AggregateRoot

from uuid import UUID

@command
class MarcarSessao:
    sessao_id: UUID
    controller_id: UUID
    inicio: datetime
    fim: datetime

@command
class CancelarSessao:
    sessao_id: UUID

@event
class SessaoMarcada:
    sessao_id: UUID
    controller_id: UUID
    inicio: datetime
    fim: datetime

@event
class SessaoCancelada:
    sessao_id: UUID

@dataclass(frozen=True)
class _Horario:
    inicio: datetime
    fim: datetime

    @classmethod
    def create(cls, inicio, fim):
        delta = fim - inicio
        if delta.seconds / 60 < 30:
            raise RuntimeError()
        return cls(inicio, fim)

    def sobreposto(self, other):
        return self.fim > other.inicio and self.inicio < other.fim

@dataclass
class Posicao(AggregateRoot):
    def __post_init__(self):
        super().__post_init__()

        self._marcacoes = dict()

    @singledispatchmethod
    def handle(self, cmd): pass

    @singledispatchmethod
    def apply(self, cmd): pass

    @handle.register
    def _(self, cmd: MarcarSessao):
        try:
            horario = _Horario.create(cmd.inicio, cmd.fim)
        except RuntimeError:
            return

        for marcacao in self._marcacoes.values():
            if horario.sobreposto(marcacao):
                return

        evt = MessageBuilder() \
            .caused_by(cmd) \
            .type(SessaoMarcada) \
            .args(cmd.sessao_id, cmd.controller_id, cmd.inicio, cmd.fim) \
            .build()
        self.apply_change(evt)

    @handle.register
    def _(self, cmd: CancelarSessao):
        if cmd.sessao_id in self._marcacoes.keys():
            evt = MessageBuilder() \
                .caused_by(cmd) \
                .type(SessaoCancelada) \
                .args(cmd.sessao_id) \
                .build()
            self.apply_change(evt)

    @apply.register
    def _(self, evt: SessaoMarcada):
        self._marcacoes[evt.sessao_id] = _Horario(evt.inicio, evt.fim)

    @apply.register
    def _(self, evt: SessaoCancelada):
        del self._marcacoes[evt.sessao_id]
