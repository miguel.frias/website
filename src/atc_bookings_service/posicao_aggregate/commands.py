from functools import singledispatchmethod

from src.kernel.repositories import Repository
from .domain import Posicao, MarcarSessao, CancelarSessao

class PosicaoCommandHandlers:
    @classmethod
    def register(cls, bus, store):
        obj = cls(Repository(Posicao, store))
        bus.register(obj.handle, MarcarSessao, CancelarSessao)
        return obj

    def __init__(self, repo: Repository):
        self.repo = repo

    @singledispatchmethod
    def handle(self, cmd): pass

    @handle.register
    def _(self, cmd: MarcarSessao):
        posicao = self.repo.get(cmd.aggregate_id)
        posicao.handle(cmd)
        self.repo.save(posicao, cmd.aggregate_version)

    @handle.register
    def _(self, cmd: CancelarSessao):
        posicao = self.repo.get(cmd.aggregate_id)
        posicao.handle(cmd)
        self.repo.save(posicao, cmd.aggregate_version)
