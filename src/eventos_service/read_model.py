from datetime import datetime
from functools import singledispatchmethod
from uuid import UUID

from .evento_aggregate import EventoCriado, ParametroEventoDefinido, EventoPublicado

class EventoListView:
    def __init__(self, db):
        db.eventos.drop()
        self.db = db.eventos

    @singledispatchmethod
    def handle(self, evt): pass

    @handle.register
    def _(self, evt: EventoCriado):
        dto = {
            'starts'    : evt.inicio,
            'ends'      : evt.fim,
            'criado_por': evt.criado_por,
            'publicado' : False,
        }
        self._update(evt, **dto)

    @handle.register
    def _(self, evt: ParametroEventoDefinido):
        self._update(evt, **{evt.nome: evt.valor})

    @handle.register
    def _(self, evt: EventoPublicado):
        self._update(evt, publicado=True)

    def _update(self, evt, **fields):
        self.db.update_one(
            {'id': evt.aggregate_id},
            {'$set': {
                'id'     : evt.aggregate_id,
                'version': evt.aggregate_version,
                **fields,
            }},
            upsert=True,
        )

class EventoReadModel:
    @classmethod
    def register(self, bus, db):
        list_view = EventoListView(db)
        evts = (EventoCriado, ParametroEventoDefinido, EventoPublicado)
        bus.register(list_view.handle, *evts)

class EventoReadFacade:
    def __init__(self, db):
        self.db = db

    def evento(self, evento_id):
        return self.db.eventos.find_one({'id': evento_id})

    def eventos(self, criado_por=None, publicado=None):
        queries = []
        if criado_por is not None:
            queries.append({'criado_por': criado_por})
        if publicado is not None:
            queries.append({'publicado': publicado, 'ends':{'$gte':datetime.utcnow()}})

        return tuple(self.db.eventos.find({'$or': queries}).sort([('starts', 1)]))
