from abc import ABC
from dataclasses import dataclass
from datetime import datetime
from functools import singledispatchmethod
from uuid import UUID

from src.kernel.messages import MessageBuilder, command, event
from src.kernel.aggregate_root import AggregateRoot

@command
class CriarEvento:
    criado_por: UUID
    inicio: datetime
    fim: datetime

@event
class EventoCriado:
    criado_por: UUID
    inicio: datetime
    fim: datetime

@command
class DefinirParametroEvento:
    nome: str
    valor: str

@event
class ParametroEventoDefinido:
    nome: str
    valor: str

@command
class PublicarEvento:
    pass

@event
class EventoPublicado:
    pass

@dataclass
class Evento(AggregateRoot):
    @singledispatchmethod
    def handle(self, cmd): pass

    @singledispatchmethod
    def apply(self, cmd): pass

    @handle.register
    def _(self, cmd: CriarEvento):
        evt = MessageBuilder() \
            .caused_by(cmd) \
            .type(EventoCriado) \
            .args(cmd.criado_por, cmd.inicio, cmd.fim) \
            .build()
        self.apply_change(evt)

    @handle.register
    def _(self, cmd: DefinirParametroEvento):
        parametro_valido = cmd.nome in self.parametros_minimos
        if not self.publicado and parametro_valido:
            evt = MessageBuilder() \
                .caused_by(cmd) \
                .type(ParametroEventoDefinido) \
                .args(cmd.nome, cmd.valor) \
                .build()
            self.apply_change(evt)

    @handle.register
    def _(self, cmd: PublicarEvento):
        parametros_minimos = self.parametros == self.parametros_minimos
        if not self.publicado and parametros_minimos:
            evt = MessageBuilder().caused_by(cmd).type(EventoPublicado).build()
            self.apply_change(evt)

    @apply.register
    def _(self, evt: EventoCriado):
        self.publicado = False
        self.parametros = set()
        self.parametros_minimos = set(['titulo', 'descricao', 'banner'])

    @apply.register
    def _(self, evt: ParametroEventoDefinido):
        self.parametros.add(evt.nome)

    @apply.register
    def _(self, evt: EventoPublicado):
        self.publicado = True
