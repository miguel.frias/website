from functools import singledispatchmethod

from src.kernel.repositories import Repository
from .domain import Evento, CriarEvento, DefinirParametroEvento, PublicarEvento

class EventoCommandHandlers:
    @classmethod
    def register(cls, bus, store):
        obj = cls(Repository(Evento, store))
        bus.register(obj.handle, CriarEvento, DefinirParametroEvento, PublicarEvento)
        return obj

    def __init__(self, repo: Repository):
        self.repo = repo

    @singledispatchmethod
    def handle(self, cmd): pass

    @handle.register
    def _(self, cmd: CriarEvento):
        evento = self.repo.get(cmd.aggregate_id)
        evento.handle(cmd)
        self.repo.save(evento, -1)

    @handle.register
    def _(self, cmd: DefinirParametroEvento):
        evento = self.repo.get(cmd.aggregate_id)
        evento.handle(cmd)
        self.repo.save(evento, cmd.aggregate_version)

    @handle.register
    def _(self, cmd: PublicarEvento):
        evento = self.repo.get(cmd.aggregate_id)
        evento.handle(cmd)
        self.repo.save(evento, cmd.aggregate_version)
